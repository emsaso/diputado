package org.democracia.diputado.service;
//package org.democracia.diputado.service;
//
//import java.lang.reflect.Method;
//import java.util.List;
//
//import org.elasticsearch.ResourceAlreadyExistsException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.codahale.metrics.annotation.Timed;
//
//@Service
//public class ElasticsearchIndexService {
//
//    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);
//    
////    private ComercioRepository comercioRepository;
////    
////    private ComercioSearchRepository comercioSearchRepository;
////    
////    private EstadoRepository estadoRepository;
////    
////    private EstadoSearchRepository estadoSearchRepository;
////    
////    private OfertaRepository ofertaRepository;
////    
////    private OfertaSearchRepository ofertaSearchRepository;
////    
////    private PaisRepository paisRepository;
////    
////    private PaisSearchRepository paisSearchRepository;
////    
////    private ProductoRepository productoRepository;
////    
////    private ProductoSearchRepository productoSearchRepository;
////    
////    private RazonSocialRepository razonSocialRepository;
////    
////    private RazonSocialSearchRepository razonSocialSearchRepository;
////    
////    private SucursalRepository sucursalRepository;
////    
////    private SucursalSearchRepository sucursalSearchRepository;
//    
//    private ElasticsearchTemplate elasticsearchTemplate;
//    
//    public ElasticsearchIndexService(
////    		ComercioRepository comercioRepository,
////		ComercioSearchRepository comercioSearchRepository, EstadoRepository estadoRepository,
////		EstadoSearchRepository estadoSearchRepository, OfertaRepository ofertaRepository,
////		OfertaSearchRepository ofertaSearchRepository, PaisRepository paisRepository,
////		PaisSearchRepository paisSearchRepository, ProductoRepository productoRepository,
////		ProductoSearchRepository productoSearchRepository, RazonSocialRepository razonSocialRepository,
////		RazonSocialSearchRepository razonSocialSearchRepository, SucursalRepository sucursalRepository,
////		SucursalSearchRepository sucursalSearchRepository, ElasticsearchTemplate elasticsearchTemplate
//		) {
////	this.comercioRepository = comercioRepository;
////	this.comercioSearchRepository = comercioSearchRepository;
////	this.estadoRepository = estadoRepository;
////	this.estadoSearchRepository = estadoSearchRepository;
////	this.ofertaRepository = ofertaRepository;
////	this.ofertaSearchRepository = ofertaSearchRepository;
////	this.paisRepository = paisRepository;
////	this.paisSearchRepository = paisSearchRepository;
////	this.productoRepository = productoRepository;
////	this.productoSearchRepository = productoSearchRepository;
////	this.razonSocialRepository = razonSocialRepository;
////	this.razonSocialSearchRepository = razonSocialSearchRepository;
////	this.sucursalRepository = sucursalRepository;
////	this.sucursalSearchRepository = sucursalSearchRepository;
////	this.elasticsearchTemplate = elasticsearchTemplate;
//}
//
//	@Async
//    @Timed
//    public void reindexAll() {
////    	reindexForClass(Comercio.class, comercioRepository, comercioSearchRepository);
////    	reindexForClass(Estado.class, estadoRepository, estadoSearchRepository);
////    	reindexForClass(Oferta.class, ofertaRepository, ofertaSearchRepository);
////    	reindexForClass(Pais.class, paisRepository, paisSearchRepository);
////    	reindexForClass(Producto.class, productoRepository, productoSearchRepository);
////    	reindexForClass(RazonSocial.class, razonSocialRepository, razonSocialSearchRepository);
////    	reindexForClass(Sucursal.class, sucursalRepository, sucursalSearchRepository);
//
//        log.info("Elasticsearch: Successfully performed reindexing");
//    }
//
//    @Async
//    @Timed
//    public void cleanAll() {
////    	cleanForClass(Comercio.class, comercioRepository, comercioSearchRepository);
////    	cleanForClass(Estado.class, estadoRepository, estadoSearchRepository);
////    	cleanForClass(Oferta.class, ofertaRepository, ofertaSearchRepository);
////    	cleanForClass(Pais.class, paisRepository, paisSearchRepository);
////    	cleanForClass(Producto.class, productoRepository, productoSearchRepository);
////    	cleanForClass(RazonSocial.class, razonSocialRepository, razonSocialSearchRepository);
////    	cleanForClass(Sucursal.class, sucursalRepository, sucursalSearchRepository);
//    }
//
//    @Transactional
//    @SuppressWarnings("unchecked")
//    private <T> void reindexForClass(Class<T> entityClass, JpaRepository<T, Long> jpaRepository,
//                                                          ElasticsearchRepository<T, Long> elasticsearchRepository) {
//        elasticsearchTemplate.deleteIndex(entityClass);
//        try {
//            elasticsearchTemplate.createIndex(entityClass);
//        } catch (ResourceAlreadyExistsException e) {
//            // Do nothing. Index was already concurrently recreated by some other service.
//        }
//        elasticsearchTemplate.putMapping(entityClass);
//        if (jpaRepository.count() > 0) {
//            try {
//                Method m = jpaRepository.getClass().getMethod("findAllWithEagerRelationships");
//                elasticsearchRepository.saveAll((List<T>) m.invoke(jpaRepository));
//            } catch (Exception e) {
//                elasticsearchRepository.saveAll(jpaRepository.findAll());
//            }
//        }
//        log.info("Elasticsearch: Indexed all rows for " + entityClass.getSimpleName());
//    }
//
//    @Transactional
//    private <T> void cleanForClass(Class<T> entityClass, JpaRepository<T, Long> jpaRepository,
//                                                          ElasticsearchRepository<T, Long> elasticsearchRepository) {
//        elasticsearchTemplate.deleteIndex(entityClass);
//    }
//
//}
