package org.democracia.diputado.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String SUPER = "ROLE_SUPER";

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String CONSULTOR = "ROLE_CONSULTOR";

//    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    
    public static final String BANCO = "ROLE_BANCO";
    
    public static final String COMERCIO = "ROLE_COMERCIO";
    
    public static final String APP = "ROLE_APP";
    
//    public static final String DEMO_COM = "ROLE_DEMO_COM";
    
    private AuthoritiesConstants() {
    }
}
