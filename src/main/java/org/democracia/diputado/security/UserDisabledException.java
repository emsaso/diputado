package org.democracia.diputado.security;

import org.springframework.security.core.AuthenticationException;

/**
 * This exception is thrown in case of a disabled user trying to authenticate.
 */
public class UserDisabledException extends AuthenticationException {
	
	private static final long serialVersionUID = 1L;

    public UserDisabledException(String message) {
        super(message);
    }

    public UserDisabledException(String message, Throwable t) {
        super(message, t);
    }

}
