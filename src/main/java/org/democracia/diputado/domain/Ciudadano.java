package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Ciudadano.
 */
@Entity
@Table(name = "ciudadano")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ciudadano implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCiudadano")
    @SequenceGenerator(name = "seqCiudadano", sequenceName="seq_ciudadano", allocationSize=1000)
    private Long id;

    @OneToMany(mappedBy = "ciudadano")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "mocion", "ciudadano" }, allowSetters = true)
    private Set<Posicion> posicions = new HashSet<>();

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "ciudadanos", "distrito", "estado" }, allowSetters = true)
    private Seccion seccion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ciudadano id(Long id) {
        this.id = id;
        return this;
    }

    public Set<Posicion> getPosicions() {
        return this.posicions;
    }

    public Ciudadano posicions(Set<Posicion> posicions) {
        this.setPosicions(posicions);
        return this;
    }

    public Ciudadano addPosicion(Posicion posicion) {
        this.posicions.add(posicion);
        posicion.setCiudadano(this);
        return this;
    }

    public Ciudadano removePosicion(Posicion posicion) {
        this.posicions.remove(posicion);
        posicion.setCiudadano(null);
        return this;
    }

    public void setPosicions(Set<Posicion> posicions) {
        if (this.posicions != null) {
            this.posicions.forEach(i -> i.setCiudadano(null));
        }
        if (posicions != null) {
            posicions.forEach(i -> i.setCiudadano(this));
        }
        this.posicions = posicions;
    }

    public Seccion getSeccion() {
        return this.seccion;
    }

    public Ciudadano seccion(Seccion seccion) {
        this.setSeccion(seccion);
        return this;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ciudadano)) {
            return false;
        }
        return id != null && id.equals(((Ciudadano) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ciudadano{" +
            "id=" + getId() +
//            ", login='" + getLogin() + "'" +
//            ", password='" + getPassword() + "'" +
            "}";
    }
}
