package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Posicion.
 */
@Entity
@Table(name = "posicion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Posicion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPosicion")
    @SequenceGenerator(name = "seqPosicion", sequenceName="seq_posicion", allocationSize=1000)
    private Long id;

    @Column(name = "posicion", nullable = false)
    private Integer posicion;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "votos", "posicions" }, allowSetters = true)
    private Mocion mocion;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "posicions", "seccion" }, allowSetters = true)
    private Ciudadano ciudadano;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Posicion id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getPosicion() {
        return this.posicion;
    }

    public Posicion posicion(Integer posicion) {
        this.posicion = posicion;
        return this;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public Mocion getMocion() {
        return this.mocion;
    }

    public Posicion mocion(Mocion mocion) {
        this.setMocion(mocion);
        return this;
    }

    public void setMocion(Mocion mocion) {
        this.mocion = mocion;
    }

    public Ciudadano getCiudadano() {
        return this.ciudadano;
    }

    public Posicion ciudadano(Ciudadano ciudadano) {
        this.setCiudadano(ciudadano);
        return this;
    }

    public void setCiudadano(Ciudadano ciudadano) {
        this.ciudadano = ciudadano;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Posicion)) {
            return false;
        }
        return id != null && id.equals(((Posicion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Posicion{" +
            "id=" + getId() +
            ", posicion=" + getPosicion() +
            "}";
    }
}
