package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Politico.
 */
@Entity
@Table(name = "politico")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Politico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPolitico")
    @SequenceGenerator(name = "seqPolitico", sequenceName="seq_politico", allocationSize=50)
    private Long id;

    @Size(max = 18)
    @Column(name = "curp", length = 18, nullable = true)
    private String curp;

    @Size(max = 63)
    @Column(name = "nombre", length = 63, nullable = false)
    private String nombre;

    @Size(max = 63)
    @Column(name = "email", length = 63, nullable = true)
    private String email;

    @Size(max = 31)
    @Column(name = "phone", length = 31, nullable = true)
    private String phone;

    @OneToMany(mappedBy = "politico")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Set<Diputado> diputados = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "politicos", "diputados" }, allowSetters = true)
    private Partido partido;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Politico id(Long id) {
        this.id = id;
        return this;
    }

    public String getCurp() {
        return this.curp;
    }

    public Politico curp(String curp) {
        this.curp = curp;
        return this;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Politico nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return this.email;
    }

    public Politico email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public Politico phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Diputado> getDiputados() {
        return this.diputados;
    }

    public Politico diputados(Set<Diputado> diputados) {
        this.setDiputados(diputados);
        return this;
    }

    public Politico addDiputado(Diputado diputado) {
        this.diputados.add(diputado);
        diputado.setPolitico(this);
        return this;
    }

    public Politico removeDiputado(Diputado diputado) {
        this.diputados.remove(diputado);
        diputado.setPolitico(null);
        return this;
    }

    public void setDiputados(Set<Diputado> diputados) {
        if (this.diputados != null) {
            this.diputados.forEach(i -> i.setPolitico(null));
        }
        if (diputados != null) {
            diputados.forEach(i -> i.setPolitico(this));
        }
        this.diputados = diputados;
    }

    public Partido getPartido() {
        return this.partido;
    }

    public Politico partido(Partido partido) {
        this.setPartido(partido);
        return this;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Politico)) {
            return false;
        }
        return id != null && id.equals(((Politico) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Politico{" +
            "id=" + getId() +
            ", curp='" + getCurp() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
