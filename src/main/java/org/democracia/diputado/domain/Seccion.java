package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Seccion.
 */
@Entity
@Table(name = "seccion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Seccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqSeccion")
    @SequenceGenerator(name = "seqSeccion", sequenceName="seq_seccion", allocationSize=50)
    private Long id;

    @Column(name = "numero", nullable = false)
    private Integer numero;

    @OneToMany(mappedBy = "seccion")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "posicions", "seccion" }, allowSetters = true)
    private Set<Ciudadano> ciudadanos = new HashSet<>();

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "seccions", "diputados", "estado" }, allowSetters = true)
    private Distrito distrito;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "distritos", "seccions", "diputados", "circunscripcion" }, allowSetters = true)
    private Estado estado;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Seccion id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Seccion numero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Set<Ciudadano> getCiudadanos() {
        return this.ciudadanos;
    }

    public Seccion ciudadanos(Set<Ciudadano> ciudadanos) {
        this.setCiudadanos(ciudadanos);
        return this;
    }

    public Seccion addCiudadano(Ciudadano ciudadano) {
        this.ciudadanos.add(ciudadano);
        ciudadano.setSeccion(this);
        return this;
    }

    public Seccion removeCiudadano(Ciudadano ciudadano) {
        this.ciudadanos.remove(ciudadano);
        ciudadano.setSeccion(null);
        return this;
    }

    public void setCiudadanos(Set<Ciudadano> ciudadanos) {
        if (this.ciudadanos != null) {
            this.ciudadanos.forEach(i -> i.setSeccion(null));
        }
        if (ciudadanos != null) {
            ciudadanos.forEach(i -> i.setSeccion(this));
        }
        this.ciudadanos = ciudadanos;
    }

    public Distrito getDistrito() {
        return this.distrito;
    }

    public Seccion distrito(Distrito distrito) {
        this.setDistrito(distrito);
        return this;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public Seccion estado(Estado estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Seccion)) {
            return false;
        }
        return id != null && id.equals(((Seccion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Seccion{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}
