package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Circunscripcion.
 */
@Entity
@Table(name = "circunscripcion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Circunscripcion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "numero", nullable = false)
    private Integer numero;

    @OneToMany(mappedBy = "circunscripcion")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "distritos", "seccions", "diputados", "circunscripcion" }, allowSetters = true)
    private Set<Estado> estados = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Circunscripcion id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Circunscripcion numero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Set<Estado> getEstados() {
        return this.estados;
    }

    public Circunscripcion estados(Set<Estado> estados) {
        this.setEstados(estados);
        return this;
    }

    public Circunscripcion addEstado(Estado estado) {
        this.estados.add(estado);
        estado.setCircunscripcion(this);
        return this;
    }

    public Circunscripcion removeEstado(Estado estado) {
        this.estados.remove(estado);
        estado.setCircunscripcion(null);
        return this;
    }

    public void setEstados(Set<Estado> estados) {
        if (this.estados != null) {
            this.estados.forEach(i -> i.setCircunscripcion(null));
        }
        if (estados != null) {
            estados.forEach(i -> i.setCircunscripcion(this));
        }
        this.estados = estados;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Circunscripcion)) {
            return false;
        }
        return id != null && id.equals(((Circunscripcion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Circunscripcion{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}
