package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Mocion.
 */
@Entity
@Table(name = "mocion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Mocion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqMocion")
    @SequenceGenerator(name = "seqMocion", sequenceName="seq_mocion", allocationSize=10)
    private Long id;

    @Size(max = 255)
    @Column(name = "nombre", length = 255, nullable = false)
    private String nombre;

    @Size(max = 1023)
    @Column(name = "descripcion", length = 1023, nullable = false)
    private String descripcion;

    @Column(name = "resultado", nullable = false)
    private Integer resultado;

    @OneToMany(mappedBy = "mocion")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "diputado", "mocion" }, allowSetters = true)
    private Set<Voto> votos = new HashSet<>();

    @OneToMany(mappedBy = "mocion")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "mocion", "ciudadano" }, allowSetters = true)
    private Set<Posicion> posicions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mocion id(Long id) {
        this.id = id;
        return this;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Mocion nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public Mocion descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getResultado() {
        return this.resultado;
    }

    public Mocion resultado(Integer resultado) {
        this.resultado = resultado;
        return this;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }

    public Set<Voto> getVotos() {
        return this.votos;
    }

    public Mocion votos(Set<Voto> votos) {
        this.setVotos(votos);
        return this;
    }

    public Mocion addVoto(Voto voto) {
        this.votos.add(voto);
        voto.setMocion(this);
        return this;
    }

    public Mocion removeVoto(Voto voto) {
        this.votos.remove(voto);
        voto.setMocion(null);
        return this;
    }

    public void setVotos(Set<Voto> votos) {
        if (this.votos != null) {
            this.votos.forEach(i -> i.setMocion(null));
        }
        if (votos != null) {
            votos.forEach(i -> i.setMocion(this));
        }
        this.votos = votos;
    }

    public Set<Posicion> getPosicions() {
        return this.posicions;
    }

    public Mocion posicions(Set<Posicion> posicions) {
        this.setPosicions(posicions);
        return this;
    }

    public Mocion addPosicion(Posicion posicion) {
        this.posicions.add(posicion);
        posicion.setMocion(this);
        return this;
    }

    public Mocion removePosicion(Posicion posicion) {
        this.posicions.remove(posicion);
        posicion.setMocion(null);
        return this;
    }

    public void setPosicions(Set<Posicion> posicions) {
        if (this.posicions != null) {
            this.posicions.forEach(i -> i.setMocion(null));
        }
        if (posicions != null) {
            posicions.forEach(i -> i.setMocion(this));
        }
        this.posicions = posicions;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mocion)) {
            return false;
        }
        return id != null && id.equals(((Mocion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Mocion{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", resultado=" + getResultado() +
            "}";
    }
}
