package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Voto.
 */
@Entity
@Table(name = "voto")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Voto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqVoto")
    @SequenceGenerator(name = "seqVoto", sequenceName="seq_voto", allocationSize=500)
    private Long id;

    @Column(name = "voto", nullable = false)
    private Integer voto;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Diputado diputado;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "votos", "posicions" }, allowSetters = true)
    private Mocion mocion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Voto id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getVoto() {
        return this.voto;
    }

    public Voto voto(Integer voto) {
        this.voto = voto;
        return this;
    }

    public void setVoto(Integer voto) {
        this.voto = voto;
    }

    public Diputado getDiputado() {
        return this.diputado;
    }

    public Voto diputado(Diputado diputado) {
        this.setDiputado(diputado);
        return this;
    }

    public void setDiputado(Diputado diputado) {
        this.diputado = diputado;
    }

    public Mocion getMocion() {
        return this.mocion;
    }

    public Voto mocion(Mocion mocion) {
        this.setMocion(mocion);
        return this;
    }

    public void setMocion(Mocion mocion) {
        this.mocion = mocion;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Voto)) {
            return false;
        }
        return id != null && id.equals(((Voto) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Voto{" +
            "id=" + getId() +
            ", voto=" + getVoto() +
            "}";
    }
}
