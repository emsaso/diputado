package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Legislatura.
 */
@Entity
@Table(name = "legislatura")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Legislatura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "numero", nullable = false)
    private Integer numero;

    @Column(name = "inicio", nullable = false)
    private LocalDate inicio;

    @Column(name = "fin", nullable = false)
    private LocalDate fin;

    @OneToMany(mappedBy = "legislatura")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Set<Diputado> diputados = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Legislatura id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Legislatura numero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public LocalDate getInicio() {
        return this.inicio;
    }

    public Legislatura inicio(LocalDate inicio) {
        this.inicio = inicio;
        return this;
    }

    public void setInicio(LocalDate inicio) {
        this.inicio = inicio;
    }

    public LocalDate getFin() {
        return this.fin;
    }

    public Legislatura fin(LocalDate fin) {
        this.fin = fin;
        return this;
    }

    public void setFin(LocalDate fin) {
        this.fin = fin;
    }

    public Set<Diputado> getDiputados() {
        return this.diputados;
    }

    public Legislatura diputados(Set<Diputado> diputados) {
        this.setDiputados(diputados);
        return this;
    }

    public Legislatura addDiputado(Diputado diputado) {
        this.diputados.add(diputado);
        diputado.setLegislatura(this);
        return this;
    }

    public Legislatura removeDiputado(Diputado diputado) {
        this.diputados.remove(diputado);
        diputado.setLegislatura(null);
        return this;
    }

    public void setDiputados(Set<Diputado> diputados) {
        if (this.diputados != null) {
            this.diputados.forEach(i -> i.setLegislatura(null));
        }
        if (diputados != null) {
            diputados.forEach(i -> i.setLegislatura(this));
        }
        this.diputados = diputados;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Legislatura)) {
            return false;
        }
        return id != null && id.equals(((Legislatura) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Legislatura{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            ", inicio='" + getInicio() + "'" +
            ", fin='" + getFin() + "'" +
            "}";
    }
}
