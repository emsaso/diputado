package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Estado.
 */
@Entity
@Table(name = "estado")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Estado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Size(max = 63)
    @Column(name = "nombre", length = 63, nullable = false)
    private String nombre;

    @OneToMany(mappedBy = "estado")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "seccions", "diputados", "estado" }, allowSetters = true)
    private Set<Distrito> distritos = new HashSet<>();

    @OneToMany(mappedBy = "estado")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ciudadanos", "distrito", "estado" }, allowSetters = true)
    private Set<Seccion> seccions = new HashSet<>();

    @OneToMany(mappedBy = "estado")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Set<Diputado> diputados = new HashSet<>();

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "estados" }, allowSetters = true)
    private Circunscripcion circunscripcion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estado id(Long id) {
        this.id = id;
        return this;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Estado nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Distrito> getDistritos() {
        return this.distritos;
    }

    public Estado distritos(Set<Distrito> distritos) {
        this.setDistritos(distritos);
        return this;
    }

    public Estado addDistrito(Distrito distrito) {
        this.distritos.add(distrito);
        distrito.setEstado(this);
        return this;
    }

    public Estado removeDistrito(Distrito distrito) {
        this.distritos.remove(distrito);
        distrito.setEstado(null);
        return this;
    }

    public void setDistritos(Set<Distrito> distritos) {
        if (this.distritos != null) {
            this.distritos.forEach(i -> i.setEstado(null));
        }
        if (distritos != null) {
            distritos.forEach(i -> i.setEstado(this));
        }
        this.distritos = distritos;
    }

    public Set<Seccion> getSeccions() {
        return this.seccions;
    }

    public Estado seccions(Set<Seccion> seccions) {
        this.setSeccions(seccions);
        return this;
    }

    public Estado addSeccion(Seccion seccion) {
        this.seccions.add(seccion);
        seccion.setEstado(this);
        return this;
    }

    public Estado removeSeccion(Seccion seccion) {
        this.seccions.remove(seccion);
        seccion.setEstado(null);
        return this;
    }

    public void setSeccions(Set<Seccion> seccions) {
        if (this.seccions != null) {
            this.seccions.forEach(i -> i.setEstado(null));
        }
        if (seccions != null) {
            seccions.forEach(i -> i.setEstado(this));
        }
        this.seccions = seccions;
    }

    public Set<Diputado> getDiputados() {
        return this.diputados;
    }

    public Estado diputados(Set<Diputado> diputados) {
        this.setDiputados(diputados);
        return this;
    }

    public Estado addDiputado(Diputado diputado) {
        this.diputados.add(diputado);
        diputado.setEstado(this);
        return this;
    }

    public Estado removeDiputado(Diputado diputado) {
        this.diputados.remove(diputado);
        diputado.setEstado(null);
        return this;
    }

    public void setDiputados(Set<Diputado> diputados) {
        if (this.diputados != null) {
            this.diputados.forEach(i -> i.setEstado(null));
        }
        if (diputados != null) {
            diputados.forEach(i -> i.setEstado(this));
        }
        this.diputados = diputados;
    }

    public Circunscripcion getCircunscripcion() {
        return this.circunscripcion;
    }

    public Estado circunscripcion(Circunscripcion circunscripcion) {
        this.setCircunscripcion(circunscripcion);
        return this;
    }

    public void setCircunscripcion(Circunscripcion circunscripcion) {
        this.circunscripcion = circunscripcion;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Estado)) {
            return false;
        }
        return id != null && id.equals(((Estado) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Estado{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            "}";
    }
}
