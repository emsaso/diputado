package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Distrito.
 */
@Entity
@Table(name = "distrito")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Distrito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqDistrito")
    @SequenceGenerator(name = "seqDistrito", sequenceName="seq_distrito", allocationSize=10)
    private Long id;

    @Column(name = "numero", nullable = false)
    private Integer numero;

    @OneToMany(mappedBy = "distrito")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ciudadanos", "distrito", "estado" }, allowSetters = true)
    private Set<Seccion> seccions = new HashSet<>();

    @OneToMany(mappedBy = "distrito")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Set<Diputado> diputados = new HashSet<>();

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "distritos", "seccions", "diputados", "circunscripcion" }, allowSetters = true)
    private Estado estado;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Distrito id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Distrito numero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Set<Seccion> getSeccions() {
        return this.seccions;
    }

    public Distrito seccions(Set<Seccion> seccions) {
        this.setSeccions(seccions);
        return this;
    }

    public Distrito addSeccion(Seccion seccion) {
        this.seccions.add(seccion);
        seccion.setDistrito(this);
        return this;
    }

    public Distrito removeSeccion(Seccion seccion) {
        this.seccions.remove(seccion);
        seccion.setDistrito(null);
        return this;
    }

    public void setSeccions(Set<Seccion> seccions) {
        if (this.seccions != null) {
            this.seccions.forEach(i -> i.setDistrito(null));
        }
        if (seccions != null) {
            seccions.forEach(i -> i.setDistrito(this));
        }
        this.seccions = seccions;
    }

    public Set<Diputado> getDiputados() {
        return this.diputados;
    }

    public Distrito diputados(Set<Diputado> diputados) {
        this.setDiputados(diputados);
        return this;
    }

    public Distrito addDiputado(Diputado diputado) {
        this.diputados.add(diputado);
        diputado.setDistrito(this);
        return this;
    }

    public Distrito removeDiputado(Diputado diputado) {
        this.diputados.remove(diputado);
        diputado.setDistrito(null);
        return this;
    }

    public void setDiputados(Set<Diputado> diputados) {
        if (this.diputados != null) {
            this.diputados.forEach(i -> i.setDistrito(null));
        }
        if (diputados != null) {
            diputados.forEach(i -> i.setDistrito(this));
        }
        this.diputados = diputados;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public Distrito estado(Estado estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Distrito)) {
            return false;
        }
        return id != null && id.equals(((Distrito) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Distrito{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}
