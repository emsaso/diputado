package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Partido.
 */
@Entity
@Table(name = "partido")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Partido implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPartido")
    @SequenceGenerator(name = "seqPartido", sequenceName="seq_partido", allocationSize=1)
    private Long id;

    @Size(max = 127)
    @Column(name = "nombre", length = 127, nullable = false)
    private String nombre;

    @Size(max = 15)
    @Column(name = "abrev", length = 15, nullable = false)
    private String abrev;

    @Size(max = 255)
    @Column(name = "logo", length = 255, nullable = true)
    private String logo;

    @OneToMany(mappedBy = "partido")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "diputados", "partido" }, allowSetters = true)
    private Set<Politico> politicos = new HashSet<>();

    @OneToMany(mappedBy = "partido")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "votos", "partido", "estado", "distrito", "legislatura", "politico" }, allowSetters = true)
    private Set<Diputado> diputados = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Partido id(Long id) {
        this.id = id;
        return this;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Partido nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbrev() {
        return this.abrev;
    }

    public Partido abrev(String abrev) {
        this.abrev = abrev;
        return this;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }

    public String getLogo() {
        return this.logo;
    }

    public Partido logo(String logo) {
        this.logo = logo;
        return this;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Set<Politico> getPoliticos() {
        return this.politicos;
    }

    public Partido politicos(Set<Politico> politicos) {
        this.setPoliticos(politicos);
        return this;
    }

    public Partido addPolitico(Politico politico) {
        this.politicos.add(politico);
        politico.setPartido(this);
        return this;
    }

    public Partido removePolitico(Politico politico) {
        this.politicos.remove(politico);
        politico.setPartido(null);
        return this;
    }

    public void setPoliticos(Set<Politico> politicos) {
        if (this.politicos != null) {
            this.politicos.forEach(i -> i.setPartido(null));
        }
        if (politicos != null) {
            politicos.forEach(i -> i.setPartido(this));
        }
        this.politicos = politicos;
    }

    public Set<Diputado> getDiputados() {
        return this.diputados;
    }

    public Partido diputados(Set<Diputado> diputados) {
        this.setDiputados(diputados);
        return this;
    }

    public Partido addDiputado(Diputado diputado) {
        this.diputados.add(diputado);
        diputado.setPartido(this);
        return this;
    }

    public Partido removeDiputado(Diputado diputado) {
        this.diputados.remove(diputado);
        diputado.setPartido(null);
        return this;
    }

    public void setDiputados(Set<Diputado> diputados) {
        if (this.diputados != null) {
            this.diputados.forEach(i -> i.setPartido(null));
        }
        if (diputados != null) {
            diputados.forEach(i -> i.setPartido(this));
        }
        this.diputados = diputados;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Partido)) {
            return false;
        }
        return id != null && id.equals(((Partido) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Partido{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", abrev='" + getAbrev() + "'" +
            ", logo='" + getLogo() + "'" +
            "}";
    }
}
