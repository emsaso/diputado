package org.democracia.diputado.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Diputado.
 */
@Entity
@Table(name = "diputado")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Diputado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqDiputado")
    @SequenceGenerator(name = "seqDiputado", sequenceName="seq_diputado", allocationSize=50)
    private Long id;

    @Column(name = "tipo", nullable = false)
    private Integer tipo;

    @Column(name = "suplido", nullable = false)
    private Boolean suplido;

    @OneToMany(mappedBy = "diputado")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "diputado", "mocion" }, allowSetters = true)
    private Set<Voto> votos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "politicos", "diputados" }, allowSetters = true)
    private Partido partido;

    @ManyToOne
    @JsonIgnoreProperties(value = { "distritos", "seccions", "diputados", "circunscripcion" }, allowSetters = true)
    private Estado estado;

    @ManyToOne
    @JsonIgnoreProperties(value = { "seccions", "diputados", "estado" }, allowSetters = true)
    private Distrito distrito;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "diputados" }, allowSetters = true)
    private Legislatura legislatura;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties(value = { "diputados", "partido" }, allowSetters = true)
    private Politico politico;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Diputado id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getTipo() {
        return this.tipo;
    }

    public Diputado tipo(Integer tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Boolean getSuplido() {
        return this.suplido;
    }

    public Diputado suplido(Boolean suplido) {
        this.suplido = suplido;
        return this;
    }

    public void setSuplido(Boolean suplido) {
        this.suplido = suplido;
    }

    public Set<Voto> getVotos() {
        return this.votos;
    }

    public Diputado votos(Set<Voto> votos) {
        this.setVotos(votos);
        return this;
    }

    public Diputado addVoto(Voto voto) {
        this.votos.add(voto);
        voto.setDiputado(this);
        return this;
    }

    public Diputado removeVoto(Voto voto) {
        this.votos.remove(voto);
        voto.setDiputado(null);
        return this;
    }

    public void setVotos(Set<Voto> votos) {
        if (this.votos != null) {
            this.votos.forEach(i -> i.setDiputado(null));
        }
        if (votos != null) {
            votos.forEach(i -> i.setDiputado(this));
        }
        this.votos = votos;
    }

    public Partido getPartido() {
        return this.partido;
    }

    public Diputado partido(Partido partido) {
        this.setPartido(partido);
        return this;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public Diputado estado(Estado estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Distrito getDistrito() {
        return this.distrito;
    }

    public Diputado distrito(Distrito distrito) {
        this.setDistrito(distrito);
        return this;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public Legislatura getLegislatura() {
        return this.legislatura;
    }

    public Diputado legislatura(Legislatura legislatura) {
        this.setLegislatura(legislatura);
        return this;
    }

    public void setLegislatura(Legislatura legislatura) {
        this.legislatura = legislatura;
    }

    public Politico getPolitico() {
        return this.politico;
    }

    public Diputado politico(Politico politico) {
        this.setPolitico(politico);
        return this;
    }

    public void setPolitico(Politico politico) {
        this.politico = politico;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diputado)) {
            return false;
        }
        return id != null && id.equals(((Diputado) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Diputado{" +
            "id=" + getId() +
            ", tipo=" + getTipo() +
            ", suplido='" + getSuplido() + "'" +
            "}";
    }
}
