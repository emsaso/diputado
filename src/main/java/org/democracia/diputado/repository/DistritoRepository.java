package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Distrito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Distrito entity.
 */
@Repository
public interface DistritoRepository extends JpaRepository<Distrito, Long> {
	
}
