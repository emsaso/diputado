package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Posicion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Posicion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PosicionRepository extends JpaRepository<Posicion, Long> {}
