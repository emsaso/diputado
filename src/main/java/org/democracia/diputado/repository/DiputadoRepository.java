package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Diputado;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Diputado entity.
 */
@Repository
public interface DiputadoRepository extends JpaRepository<Diputado, Long> {
	
	@Query("SELECT d FROM Diputado d WHERE d.distrito.id = :distritoId")
	Diputado getByDistrito(@Param("distritoId") Long distritoId);
}
