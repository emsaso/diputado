package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Distrito;
import org.democracia.diputado.domain.Seccion;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Seccion entity.
 */
@Repository
public interface SeccionRepository extends JpaRepository<Seccion, Long> {
	
	@Query("SELECT s.distrito FROM Seccion s WHERE s.estado.id = :estadoId AND s.numero = :numero")
	Distrito getDistritoByEstadoAndSeccion(@Param("estadoId") Long estadoId, @Param("numero") Integer numero);
}
