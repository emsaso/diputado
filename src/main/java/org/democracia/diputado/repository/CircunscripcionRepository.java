package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Circunscripcion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Circunscripcion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CircunscripcionRepository extends JpaRepository<Circunscripcion, Long> {}
