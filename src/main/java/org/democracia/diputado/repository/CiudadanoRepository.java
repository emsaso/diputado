package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Ciudadano;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Ciudadano entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CiudadanoRepository extends JpaRepository<Ciudadano, Long> {}
