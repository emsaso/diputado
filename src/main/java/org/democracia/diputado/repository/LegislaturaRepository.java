package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Legislatura;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Legislatura entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LegislaturaRepository extends JpaRepository<Legislatura, Long> {}
