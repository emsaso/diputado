package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Mocion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Mocion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MocionRepository extends JpaRepository<Mocion, Long> {}
