package org.democracia.diputado.repository;

import org.democracia.diputado.domain.Politico;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Politico entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PoliticoRepository extends JpaRepository<Politico, Long> {}
