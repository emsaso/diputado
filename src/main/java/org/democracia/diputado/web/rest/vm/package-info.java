/**
 * View Models used by Spring MVC REST controllers.
 */
package org.democracia.diputado.web.rest.vm;
