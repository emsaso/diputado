package org.democracia.diputado.web.rest;

import java.util.List;

import org.democracia.diputado.domain.Diputado;
import org.democracia.diputado.domain.Distrito;
import org.democracia.diputado.domain.Estado;
import org.democracia.diputado.repository.DiputadoRepository;
import org.democracia.diputado.repository.EstadoRepository;
import org.democracia.diputado.repository.SeccionRepository;
import org.democracia.diputado.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

/**
 * REST controller for managing public web services.
 */
@RestController
@RequestMapping("/public")
public class PublicResource {
	
	private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final DiputadoRepository diputadoRepository;

    private final EstadoRepository estadoRepository;

    private final SeccionRepository seccionRepository;
    
    public PublicResource(DiputadoRepository diputadoRepository, EstadoRepository estadoRepository, SeccionRepository seccionRepository) {
    	this.diputadoRepository = diputadoRepository;
    	this.estadoRepository = estadoRepository;
    	this.seccionRepository = seccionRepository;
    }
    
	/**
     * GET  /dipBySeccion : Obtener datos de contacto del Diputado asiganado a la Seccion Electoral
     *
     * @param estadoId identificador del estado
     * @param numero numero de la seccion electoral
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/dipBySeccion")
    @Timed
    public ResponseEntity<Diputado> getBySeccion(@RequestParam(value = "estadoId") Long estadoId, @RequestParam(value = "seccion") Integer seccion) {
    	
    	Distrito dist = seccionRepository.getDistritoByEstadoAndSeccion(estadoId, seccion);
    	if(dist==null)
    		return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(null, "seccionNotFound", "Seccion invalida"))
					.body(null);
    	
    	Diputado dip = diputadoRepository.getByDistrito(dist.getId());
    	if(dip==null)
    		return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(null, "dipNotFound", "No se encontraron datos para la consulta"))
					.body(null);
    	
    	return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(null, null))
				.body(dip);
    }
    
	/**
     * GET  /estados : Obtener lista de Estados
     *
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/estados")
    @Timed
    public ResponseEntity<List<Estado>> estados() {
    	
    	List<Estado> estados = estadoRepository.findAll();
    	
    	return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(null, null))
				.body(estados);
    }

}
