package org.democracia.diputado.config;

import java.time.Duration;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(org.democracia.diputado.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(org.democracia.diputado.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Circunscripcion.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Circunscripcion.class.getName() + ".estados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Estado.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Estado.class.getName() + ".distritos", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Estado.class.getName() + ".seccions", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Estado.class.getName() + ".diputados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Distrito.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Distrito.class.getName() + ".seccions", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Distrito.class.getName() + ".diputados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Seccion.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Seccion.class.getName() + ".ciudadanos", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Legislatura.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Legislatura.class.getName() + ".diputados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Partido.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Partido.class.getName() + ".politicos", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Partido.class.getName() + ".diputados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Diputado.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Diputado.class.getName() + ".votos", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Politico.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Politico.class.getName() + ".diputados", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Mocion.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Mocion.class.getName() + ".votos", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Mocion.class.getName() + ".posicions", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Voto.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Ciudadano.class.getName(), jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Ciudadano.class.getName() + ".posicions", jcacheConfiguration);
            cm.createCache(org.democracia.diputado.domain.Posicion.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
