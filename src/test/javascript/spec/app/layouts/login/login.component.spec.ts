import { ActivateService } from 'app/account/activate/activate.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { PasswordResetInitService } from 'app/account/password-reset/init/password-reset-init.service';
import { LoginService } from 'app/core/login/login.service';
import { PanelComponent } from 'app/layouts/panel/panel.component';
import { errorRoute } from 'app/shared/components/error/error.route';
import { loginRoute } from 'app/layouts/login/login.route';
import { panelRoute } from 'app/layouts/panel/panel.route';
import { of } from 'rxjs/observable/of';
import { RouterModule, Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { MatDialogModule, MatSidenavModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { LoginComponent, ErrorComponent, SidebarComponent } from 'app/layouts';
import { diputadoTestModule } from '../../../test.module';
import { NgJhipsterModule } from 'ng-jhipster';
import { ReactiveFormsModule } from '@angular/forms';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { RouterTestingModule } from '@angular/router/testing';
import { VplusHomeComponent } from 'app/layouts/vplus-home/vplus-home.component';
import { HeaderComponent } from 'app/shared/components/header/header.component';
import { FunnelSeriesDataItem } from '@amcharts/amcharts4/charts';
import { throwError } from 'rxjs';

describe('Login Component Tests', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let loginService: LoginService;
    let passwordResetInitService: PasswordResetInitService;
    let activateService: ActivateService;
    let stateStorageService: StateStorageService;
    let router: Router;

    const neededParamForActivationProcess = 'key';
    const testActivationKey = 'TheKey';
    const failLoginTest = 'test@gmail.com';
    const failPasswordTest = 'testPassword';
    const failEmailTest = 'test@nonexistent.com';
    const failEmailPatternTest = 'badmail.com';
    const successLoginTest = 'admin';
    const successPasswordTest = 'admin';
    const successEmailTest = 'consultor@gmail.com';

    const mockFailedLoginResponse = {
        type: 'https://www.jhipster.tech/problem/problem-with-message',
        title: 'Unauthorized',
        status: 401,
        detail: 'Bad credentials',
        path: '/api/authenticate',
        message: 'error.http.401'
    };

    const mockSuccessLoginResponse = {
        id_token: 'eyJGciOiUxMiJ9.eyJzdWIiOiJhZG1pbiIsI'
    };
    const mockFailedResetResponse = {
        type: 'https://www.jhipster.tech/problem/email-not-found',
        title: 'Email address not registered',
        status: 400
    };
    const mockSuccessResetResponse = null;

    const mockActivationErrorResponse = {
        type: 'https://www.jhipster.tech/problem/problem-with-message',
        title: 'No user was found for this activation key',
        status: 500
    };

    // let stateStorageService: StateStorageService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                diputadoTestModule,
                RouterTestingModule.withRoutes([loginRoute]),
                MatDialogModule,
                MatSidenavModule,
                ReactiveFormsModule,
                CommonModule,
                NgbModule.forRoot(),
                NgJhipsterModule.forRoot({
                    // set below to true to make alerts look like toast
                    alertAsToast: false,
                    i18nEnabled: true,
                    defaultI18nLang: 'es'
                })
            ],

            declarations: [LoginComponent],
            providers: [
                { provide: LocalStorageService },
                { provide: SessionStorageService }
                // { provide: Router, useValue: routerSpy }
                // { provide: Router},
                // { provide: ActivatedRoute, useValue: {params: of([{id: 1}]),}},
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.debugElement.componentInstance;
        loginService = fixture.debugElement.injector.get(LoginService);
        passwordResetInitService = fixture.debugElement.injector.get(PasswordResetInitService);
        activateService = fixture.debugElement.injector.get(ActivateService);
        stateStorageService = fixture.debugElement.injector.get(StateStorageService);
        router = fixture.debugElement.injector.get(Router);
    });

    describe('DOM Tests', () => {
        describe('OnInit', () => {
            it(
                'Should show username input on initialization',
                async(() => {
                    //GIVEN

                    //WHEN
                    fixture.detectChanges();

                    //THEN
                    const element = fixture.debugElement.query(By.css('#username-input'));
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );

            it(
                'Should show password input on initialization',
                async(() => {
                    //GIVEN

                    //WHEN
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#password-input'));
                    //THEN
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );

            it(
                'Should hide email input on initalization',
                async(() => {
                    //GIVEN

                    //WHEN
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#email-input'));
                    //THEN
                    if (element == null) {
                        expect(element).toBeFalsy();
                    }
                })
            );
        });

        describe('On setting default layout', () => {
            it(
                'Should show username input on default layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //THEN
                    const element = fixture.debugElement.query(By.css('#username-input'));
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );

            it(
                'Should show password input on default layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#password-input'));
                    //THEN
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );

            it(
                'Should hide email input on default layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#email-input'));
                    //THEN
                    expect(element).toBeFalsy();
                })
            );

            it(
                'Should reset email on default layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //THEN
                    const emailControl = component.formGroup.get('email');
                    const value = emailControl.value;
                    const validators = emailControl.validator;
                    const errors = emailControl.errors;

                    expect(value).toMatch('');
                    expect(validators).toBeNull();
                    expect(errors).toBeNull();
                })
            );
        });

        describe('On setting restablecer layout', () => {
            it(
                'Should hide username input on restablecer layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();
                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //THEN
                    const element = fixture.debugElement.query(By.css('#username-input'));
                    expect(element).toBeFalsy();
                })
            );

            it(
                'Should reset username on restablecerLayout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //THEN
                    const usernameControl = component.formGroup.get('username');
                    const value = usernameControl.value;
                    const validators = usernameControl.validator;
                    const errors = usernameControl.errors;

                    expect(value).toMatch('');
                    expect(validators).toBeNull();
                    expect(errors).toBeNull();
                })
            );

            it(
                'Should hide password input on restablecer layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#password-input'));
                    //THEN
                    expect(element).toBeFalsy();
                })
            );

            it(
                'Should reset password on restablecerLayout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    //THEN
                    const passwordControl = component.formGroup.get('password');
                    const value = passwordControl.value;
                    const validators = passwordControl.validator;
                    const errors = passwordControl.errors;

                    expect(value).toMatch('');
                    expect(validators).toBeNull();
                    expect(errors).toBeNull();
                })
            );

            it(
                'Should show email input on restablecer layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#email-input'));
                    //THEN
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );
            it(
                'Should show email input on restablecer layout',
                async(() => {
                    //GIVEN
                    component.ngOnInit();
                    fixture.detectChanges();
                    component.setDefaultLayout();
                    fixture.detectChanges();

                    //WHEN
                    component.setRestablecerLayout();
                    fixture.detectChanges();

                    const element = fixture.debugElement.query(By.css('#email-input'));
                    //THEN
                    expect(element.nativeElement.hasAttribute('hidden')).toBeFalsy();
                })
            );
        });
    });

    describe('On Login', () => {
        it(
            'Should not call login() on pristine form group',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should not call login() on empty username',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('password').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should not call login() on empty password',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('username').setValue(failLoginTest);
                component.formGroup.updateValueAndValidity();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should call login() on default Layout and full data',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setDefaultLayout();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('username').setValue(failLoginTest);
                component.formGroup.get('password').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).toHaveBeenCalledTimes(1);
            })
        );
        it(
            'Should call login() on restablecer Layout and full data',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setDefaultLayout();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('username').setValue(failLoginTest);
                component.formGroup.get('password').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();

                component.setRestablecerLayout();
                fixture.detectChanges();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should call login() with correct parameters',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();

                component.formGroup.markAsDirty();
                component.formGroup.get('username').setValue(failLoginTest);
                component.formGroup.get('password').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();
                fixture.detectChanges();
                const loginSpy = spyOn(component, 'login').and.callThrough();

                //WHEN
                component.sendData();

                //THEN
                expect(loginSpy).toHaveBeenCalledTimes(1);
                expect(loginSpy).toHaveBeenLastCalledWith(component.formGroup.value);
                // const element = fixture.debugElement.query(By.css('#password-input'))
            })
        );

        it('Should call router navigate on loginObservable success response and no previous state', async done => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();

            const loginObservableSpy = spyOn(loginService, 'loginObservable').and.returnValue(of(mockSuccessLoginResponse));
            const stateStorageServiceGetUrlSpy = spyOn(stateStorageService, 'getUrl').and.returnValue(null);
            const routerNavigateSpy = spyOn(router, 'navigate').and.callFake((commands: any[], extras?: NavigationExtras) => {});
            const loginDTO = { username: successLoginTest, password: successPasswordTest };

            //WHEN
            component.login(loginDTO);

            //THEN
            expect(routerNavigateSpy).toHaveBeenCalledWith(['../panel'], expect.anything());

            done();
        });

        it('Should call processError() on loginObservable error response', async done => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();

            const loginObservableSpy = spyOn(loginService, 'loginObservable').and.returnValue(throwError(mockFailedLoginResponse));
            const processErrorSpy = spyOn(component, 'processError').and.callThrough();
            const loginDTO = { username: failLoginTest, password: failPasswordTest };

            //WHEN
            component.login(loginDTO);

            //THEN

            expect(processErrorSpy).toHaveBeenCalledTimes(1);
            expect(processErrorSpy).toHaveBeenLastCalledWith(mockFailedLoginResponse);

            done();
            // expect(passwordResetSpy).toHaveBeenLastCalledWith(failEmailTest);
        });
    });

    describe('On Reset Request', () => {
        it(
            'Should not call requestReset() on pristine form group and restablecer layout',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();
                const requestResetSpy = spyOn(component, 'requestReset').and.callThrough();

                //WHEN
                component.sendData();

                //THEN
                expect(requestResetSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should not call requestReset() on empty email and restablecer layout',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();
                const requestResetSpy = spyOn(component, 'requestReset').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('email').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();

                //WHEN
                component.sendData();

                //THEN
                expect(requestResetSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should call requestReset() restablecer layout and full data',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();
                const requestResetSpy = spyOn(component, 'requestReset').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('email').setValue(failEmailTest);
                component.formGroup.updateValueAndValidity();

                //WHEN
                component.sendData();

                //THEN
                expect(requestResetSpy).toHaveBeenCalledTimes(1);
            })
        );
        it(
            'Should not call requestReset() on default layout and full data',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();
                const requestResetSpy = spyOn(component, 'requestReset').and.callThrough();

                component.formGroup.markAsDirty();
                component.formGroup.get('username').setValue(failLoginTest);
                component.formGroup.get('password').setValue(failPasswordTest);
                component.formGroup.updateValueAndValidity();

                component.setDefaultLayout();
                fixture.detectChanges();

                //WHEN
                component.sendData();

                //THEN
                expect(requestResetSpy).not.toHaveBeenCalled();
            })
        );

        it(
            'Should call requestReset() with correct parameters',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();

                component.formGroup.markAsDirty();
                component.formGroup.get('email').setValue(failEmailTest);
                component.formGroup.updateValueAndValidity();
                fixture.detectChanges();
                const requestResetSpy = spyOn(component, 'requestReset').and.callThrough();

                //WHEN
                component.sendData();

                //THEN
                expect(requestResetSpy).toHaveBeenCalledTimes(1);
                expect(requestResetSpy).toHaveBeenLastCalledWith(failEmailTest);
                // const element = fixture.debugElement.query(By.css('#password-input'))
            })
        );

        it(
            'Should call passwordResetInitService save() called with correct parameters',
            async(done => {
                //GIVEN
                component.ngOnInit();
                fixture.detectChanges();
                component.setRestablecerLayout();
                fixture.detectChanges();

                component.formGroup.get('email').setValue(failEmailTest);
                const passwordResetSpy = spyOn(passwordResetInitService, 'save').and.callThrough();

                //WHEN
                component.requestReset(component.formGroup.value.email);

                //THEN
                expect(passwordResetSpy).toHaveBeenCalledTimes(1);
                expect(passwordResetSpy).toHaveBeenLastCalledWith(failEmailTest);
                // const element = fixture.debugElement.query(By.css('#password-input'))
            })
        );
        it('Should call processRequestSuccess() on  passwordResetInitService save() success response', async done => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();
            component.setRestablecerLayout();
            fixture.detectChanges();

            spyOn(passwordResetInitService, 'save').and.returnValue(of(mockSuccessResetResponse));
            const processRequestSuccessSpy = spyOn(component, 'processRequestSuccess').and.callThrough();

            //WHEN
            component.requestReset(successEmailTest);

            //THEN

            expect(processRequestSuccessSpy).toHaveBeenCalledTimes(1);

            done();
            // expect(passwordResetSpy).toHaveBeenLastCalledWith(failEmailTest);
        });
        it('Should call processError() on  passwordResetInitService save() error response', async done => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();
            component.setRestablecerLayout();
            fixture.detectChanges();

            spyOn(passwordResetInitService, 'save').and.returnValue(throwError(mockFailedResetResponse));
            const processErrorSpy = spyOn(component, 'processError').and.callThrough();

            //WHEN
            component.requestReset(failEmailTest);

            //THEN

            expect(processErrorSpy).toHaveBeenCalledTimes(1);
            expect(processErrorSpy).toHaveBeenLastCalledWith(mockFailedResetResponse);

            done();
            // expect(passwordResetSpy).toHaveBeenLastCalledWith(failEmailTest);
        });
    });

    describe.skip('On Activate Account', () => {
        it('Should call checkoutRouteParams on Initialization process', () => {
            //GIVEN

            const functionSpy = spyOn(component, 'checkRouteParams').and.callThrough();

            //WHEN
            component.ngOnInit();
            fixture.detectChanges();

            //THEN
            expect(functionSpy).toHaveBeenCalled();
        });

        it('Should call activateAccount on activation param present', () => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();
            const functionSpy = spyOn(component, 'activateAccount').and.callThrough();

            //WHEN

            component.checkRouteParams({ [neededParamForActivationProcess]: testActivationKey });
            fixture.detectChanges();

            //THEN
            expect(functionSpy).toHaveBeenCalled();
            expect(functionSpy).toHaveBeenCalledWith(testActivationKey);
        });

        it('Should call errorOnConfirmLink on activateAccount and activationService.get() error response', () => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();

            spyOn(activateService, 'get').and.returnValue(throwError(mockActivationErrorResponse));
            const functionSpy = spyOn(component, 'errorOnConfirmLink').and.callThrough();

            //WHEN
            component.activateAccount(testActivationKey);

            //THEN
            expect(functionSpy).toHaveBeenCalled();
            expect(functionSpy).toHaveBeenCalledWith(mockActivationErrorResponse);
        });

        it('Should call resultOnConfirmLink with correct params on errorOnConfirmLink', () => {
            //GIVEN
            component.ngOnInit();
            fixture.detectChanges();

            const functionSpy = spyOn(component, 'resultOnConfirmLink').and.callFake((...args: any[]) => {});

            //WHEN
            component.errorOnConfirmLink(mockActivationErrorResponse);
            fixture.detectChanges();

            //THEN
            expect(functionSpy).toHaveBeenCalled();
            expect(functionSpy).toHaveBeenCalledWith(mockActivationErrorResponse, false);
        });
    });
});
