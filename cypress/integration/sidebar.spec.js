describe ('Sidebar Admin', () => {
    const defaultAdminHomeRoute = '/panel/home';
    const profileRoute = '/panel/perfil';
    const userManagementRoute = '/panel/admin/user-management';
//    const bancosRoute = '/panel/bancos';
//    const comerciosRoute = '/panel/comercios';

    before(() => {
        //GIVEN
        cy.visit('/')
        cy.fixture('loginData.json').as('loginData')
        cy.get('@loginData').then((loginData) => {
            cy.loginUser(loginData.username, loginData.password)
            
        })

        //WHEN

        //THEN
        //THEN
        cy.url().should('eq', `http://localhost:8080/#${defaultAdminHomeRoute}`)

    })

    it('Should hide sidebar by default', () => {
        //GIVEN
        //WHEN

        //THEN
        cy.get('.sidebar').should('not.be.visible')
    })

    it('Should show sidebar on toggle click and sidebar hidden', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('.toggleButton').click()


        //THEN
        cy.get('.sidebar').should('be.visible')
    })

    it('Should hide sidebar on toggle click and sidebar visible', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('.toggleButton').click()
        cy.wait(500)
        cy.get('.toggleButton').click()


        //THEN
        cy.get('.sidebar').should('not.be.visible')
    })

    it('Should show diputado sidebar button', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('.toggleButton').click()

        //THEN
        cy.contains('diputado').should('be.visible')
    })

    it('Should show Bancos sidebar button', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('.toggleButton').click()

        //THEN
        cy.contains('Bancos').should('be.visible')
    })

    it('Should show Comercios sidebar button', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('.toggleButton').click()

        //THEN
        cy.contains('Comercios').should('be.visible')
    })

    it('Should redirect to user-management panel on diputado button click', () => {
        //GIVEN
        cy.reload()
        
        //WHEN
        cy.get('.toggleButton').click()
        cy.contains('diputado').click()

        //THEN
        cy.url().should('eq', `http://localhost:8080/#${userManagementRoute}`)
    })

//    it('Should redirect to bancos panel on Bancos button click', () => {
//        //GIVEN
//        cy.reload()
//        
//        //WHEN
//        cy.get('.toggleButton').click()
//        cy.contains('Bancos').click()
//
//        //THEN
//        cy.url().should('eq', `http://localhost:8080/#${bancosRoute}`)
//    })

//    it('Should redirect to comercios panel on Comercios button click', () => {
//        //GIVEN
//        cy.reload()
//        
//        //WHEN
//        cy.get('.toggleButton').click()
//        cy.contains('Comercios').click()
//
//        //THEN
//        cy.url().should('eq', `http://localhost:8080/#${comerciosRoute}`)
//    })


})