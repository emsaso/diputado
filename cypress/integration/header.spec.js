

describe('Header Admin', () => {

    const defaultAdminHomeRoute = '/panel/home';
    const profileRoute = '/panel/perfil';

    before(() => {
        //GIVEN
        cy.visit('/')
        cy.fixture('loginData.json').as('loginData')
        cy.get('@loginData').then((loginData) => {
            cy.loginUser(loginData.username, loginData.password)
            
        })

        //WHEN

        //THEN
        //THEN
        cy.url().should('eq', `http://localhost:8080/#${defaultAdminHomeRoute}`)   

    })


    it('Should show toggle on header', () => {
        //GIVEN
        //WHEN

        //THEN
        cy.get('.toggleButton').should('be.visible')
    })

    it('Should show diputado home button on header', () => {
        //GIVEN
        //WHEN
        
        //THEN
        cy.get('#home-button').should('be.visible')
    })

    it('Should redirect to vphome on home-button click', () => {
        //GIVEN
        //WHEN
        cy.get('#home-button').click()
        cy.wait(500)
        
        //THEN
        cy.url().should('eq', `http://localhost:8080/#${defaultAdminHomeRoute}`)
    })

    it('Should show config button on header', () => {
        //GIVEN
        //WHEN
        
        //THEN
        cy.get('#config-button').should('be.visible')
    })

    it('Should show profile button on config button click', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('#config-button').click()
        cy.wait(500)
        
        //THEN
        cy.get('#profile-button').should('be.visible')

    })

    it('Should show logout button on config button click', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('#config-button').click()
        cy.wait(500)
        
        //THEN
        cy.get('#logout-button').should('be.visible')

    })

    it('Should go to profile page on profile button click', () => {
        //GIVEN
        cy.reload()

        //WHEN
        cy.get('#config-button').click()
        cy.wait(500)
        cy.get('#profile-button').click()
        cy.wait(500)
        
        //THEN
        cy.url().should('eq', `http://localhost:8080/#${profileRoute}`)   

    })
    it('Should go to login page on logout button click', () => {
        //GIVEN
        cy.reload()
        
        //WHEN
        cy.get('#config-button').click()
        cy.wait(500)
        cy.get('#logout-button').click()
        cy.wait(500)
        
        //THEN
        cy.url().should('eq', `http://localhost:8080/#/`)   

    })
})

