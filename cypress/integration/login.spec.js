

describe('Nav Login', () => {

    const failLoginTest = 'test@gmail.com';
    const failPasswordTest = 'testPassword';

    beforeEach(() => {
        //GIVEN
        cy.visit('/')

        //WHEN
        //THEN
        
    
    })

    it('Should access login page', () => {
        //THEN
        cy.contains('.welcome-message', 'Bienvenido').should('be.visible')        
    })

    it('Should hide username error message on start', () => {
        //THEN
        cy.get('#username-required').should('be.not.visible')

    })
    it('Should hide password error message on start', () => {
        //THEN
        cy.get('#password-required').should('be.not.visible')

    })

    it('Should username and password errors on login on empty login data', () => {
        //WHEN
        cy.get('#form-button').click()
        cy.wait(500)


        //THEN
        cy.get('#username-required').should('be.visible')
        cy.get('#password-required').should('be.visible')
        
    })

    it('Should show username error on form dirty and empty input', () => {
        //WHEN
        cy.get('#username-input').type(failLoginTest);
        cy.wait(500);
        cy.get('#username-input').clear();
        cy.wait(500);

        //THEN
        cy.get('#username-required').should('be.visible')
        
        
    })
    it('Should show password error on form dirty and empty input', () => {
        //WHEN
        cy.get('#password-input').type(failLoginTest);
        cy.wait(500);
        cy.get('#password-input').clear();
        cy.wait(500);

        //THEN
        cy.get('#password-required').should('be.visible')
        
    })


    it('Should throw error on failed login data', () => {
        //WHEN
        cy.loginUser(failLoginTest, failPasswordTest)
        cy.wait(500)

        //THEN
        cy.get('.alert-danger').should('have.length.greaterThan', 0)
    })


    it('Should login', () => {


        cy.fixture('loginData.json').as('loginData')

        cy.get('@loginData').then((loginData) => {
            //WHEN
            cy.loginUser(loginData.username, loginData.password)

            //THEN
            cy.url().should('eq', 'http://localhost:8080/#' + '/panel/home')
            
        })

        
    })

    
})

describe.skip('Reestablish password', () => {

    beforeEach(() => {
        //GIVEN
        cy.visit('/')
        cy.get('#change-layout-button').click()
    })

    it.skip('Should show layout to reestablish password', () => {
        // cy.visit('/')
        // cy.contains('a','Reestablecer contraseña').click()
        // cy.wait(500)

        // // cy.contains('.welcome-message', 'Reestablecer Contraseña').should('be.visible')
        // cy.contains('.text-center', 'Introduzca la dirección de correo electrónico que utilizó para registrarse').should('be.visible')
        // cy.contains('.login-subtitle', 'Ingrese Correo Electronico').should('be.visible')
        // cy.get('#email').should('be.visible')
        // cy.contains('#login-button', 'Resetear').should('be.visible')
        // cy.contains('a','Regresar').should('be.visible')

    })
})
